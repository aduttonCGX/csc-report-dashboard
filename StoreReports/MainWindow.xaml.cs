﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StoreReports.Utilities;
using StoreReports.Reporting;
using System.Data.SqlClient;
using System.Data;

namespace StoreReports
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool buttonDepressed = false;
        private List<Store> stores;
        private Session session;

        public MainWindow()
        {
            InitializeComponent();
            session = new Session();
            session.LoadFromFile();

            // Populate store dropdown
            GetStores();
            if (string.IsNullOrWhiteSpace(session.LocationCode))
            {
                SetDefaultStore();
            }
            else
            {
                cboxSelectedStore.SelectedValue = session.LocationCode;
            }            

            // Set Default Dates
            dpPOReceiptDateFrom.SelectedDate = DateTime.Now;
            dpPOReceiptDateTo.SelectedDate = DateTime.Now;
            dpVMICloseDate.SelectedDate = DateTime.Now;
            dpTransferReceiveReceiveDateFrom.SelectedDate = DateTime.Now.AddDays(-6);
            dpTransferReceiveReceiveDateTo.SelectedDate = DateTime.Now;
            dpTransferShipCreateDateFrom.SelectedDate = DateTime.Now.AddDays(-6);
            dpTransferShipCreateDateTo.SelectedDate = DateTime.Now;
            dpRTVShipDateFrom.SelectedDate = DateTime.Now.AddDays(-6);
            dpRTVShipDateTo.SelectedDate = DateTime.Now;
            dpVMICreateDateFrom.SelectedDate = DateTime.Now.AddDays(-6);
            dpVMICreateDateTo.SelectedDate = DateTime.Now;

            // Collapse Reports
            
            pnlPurchaseOrderCollapsable.Visibility = (session.PurchaseOrderReportShown) ? Visibility.Visible : Visibility.Collapsed;
            pnlTransferReceiveCollapsable.Visibility = (session.TransferReceiptReportShown) ? Visibility.Visible : Visibility.Collapsed;
            pnlTransferShipCollapsable.Visibility = (session.TransferShipReportShown) ? Visibility.Visible : Visibility.Collapsed;
            pnlVMICollapsable.Visibility = (session.VMIRegisterReportShown) ? Visibility.Visible : Visibility.Collapsed;
            pnlWeeklyVMICollapsable.Visibility = (session.VMIWeeklyReportShown) ? Visibility.Visible : Visibility.Collapsed;
            pnlRTVCollapsable.Visibility = (session.RTVReportShown) ? Visibility.Visible : Visibility.Collapsed;
            this.Height = session.Height;
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void DisableButtons()
        {
            btnPurchaseOrderReport.IsEnabled = false;
            btnVMIReport.IsEnabled = false;
            btnTransferReceiveReport.IsEnabled = false;
            btnTransferShipReport.IsEnabled = false;
            btnVMIReport.IsEnabled = false;
        }

        private void EnableButtons()
        {
            btnPurchaseOrderReport.IsEnabled = true;
            btnVMIReport.IsEnabled = true;
            btnTransferReceiveReport.IsEnabled = true;
            btnTransferShipReport.IsEnabled = true;
            btnVMIReport.IsEnabled = true;
        }

        private void BtnPurchaseOrderReport_Click(object sender, RoutedEventArgs e)
        {
            if (cboxSelectedStore.SelectedIndex == -1)
            {
                MBWindow.Show("Please select a store.", "Error", Utilities.MessageBoxButton.OK);
                return;
            }   

            PurchaseOrderReport purchaseOrderReport = new PurchaseOrderReport(cboxSelectedStore.SelectedValue.ToString(), dpPOReceiptDateFrom.SelectedDate.Value, dpPOReceiptDateTo.SelectedDate.Value);
            ReportGenerationWindow reportGenerationWindow = new ReportGenerationWindow(purchaseOrderReport);
            reportGenerationWindow.ShowDialog();
        }

        private void BtnVMIReport_Click(object sender, RoutedEventArgs e)
        {
            if (cboxSelectedStore.SelectedIndex == -1)
            {
                MBWindow.Show("Please select a store.", "Error", Utilities.MessageBoxButton.OK);
                return;
            }

            VMIReport vmiReport = new VMIReport(cboxSelectedStore.SelectedValue.ToString(), dpVMICloseDate.SelectedDate.Value);
            ReportGenerationWindow reportGenerationWindow = new ReportGenerationWindow(vmiReport);
            reportGenerationWindow.ShowDialog();
        }

        private void BtnTransferReceiveReport_Click(object sender, RoutedEventArgs e)
        {
            if (cboxSelectedStore.SelectedIndex == -1)
            {
                MBWindow.Show("Please select a store.", "Error", Utilities.MessageBoxButton.OK);
                return;
            }

            TransferReceiptReport transferReceiptReport = new TransferReceiptReport(cboxSelectedStore.SelectedValue.ToString(), dpTransferReceiveReceiveDateFrom.SelectedDate.Value, dpTransferReceiveReceiveDateTo.SelectedDate.Value);
            ReportGenerationWindow reportGenerationWindow = new ReportGenerationWindow(transferReceiptReport);
            reportGenerationWindow.ShowDialog();
        }

        private void BtnTransferShipReport_Click(object sender, RoutedEventArgs e)
        {
            if (cboxSelectedStore.SelectedIndex == -1)
            {
                MBWindow.Show("Please select a store.", "Error", Utilities.MessageBoxButton.OK);
                return;
            }

            TransferShipReport transferShipReport = new TransferShipReport(cboxSelectedStore.SelectedValue.ToString(), dpTransferShipCreateDateFrom.SelectedDate.Value, dpTransferShipCreateDateTo.SelectedDate.Value);
            ReportGenerationWindow reportGenerationWindow = new ReportGenerationWindow(transferShipReport);
            reportGenerationWindow.ShowDialog();
        }

        private void BtnRTVReport_Click(object sender, RoutedEventArgs e)
        {
            if (cboxSelectedStore.SelectedIndex == -1)
            {
                MBWindow.Show("Please select a store.", "Error", Utilities.MessageBoxButton.OK);
                return;
            }

            RTVReport rtvReport = new RTVReport(cboxSelectedStore.SelectedValue.ToString(), dpRTVShipDateFrom.SelectedDate.Value, dpRTVShipDateTo.SelectedDate.Value);            
            if (rbRTVCriteriaCreateDate.IsChecked == true)
            {
                rtvReport.Basis = RTVReportBasis.CreateDate;
            }
            ReportGenerationWindow reportGenerationWindow = new ReportGenerationWindow(rtvReport);
            reportGenerationWindow.ShowDialog();

        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            session.SaveToFile();
            this.Close();
        }

        private void GetStores()
        {
            stores = new List<Store>();
            stores.Add(new Store("ALL", "All Stores"));
            string query = @"SELECT [Code], [Description] FROM [mi9_mms_cg_live].[dbo].[ar_v_Stores]  WHERE [Status]='O' AND [Code] NOT IN('097', '001', '900', 'W01') ORDER BY [Description]";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    MBWindow.Show($"Connection open failed: {ex.Message}", "Error", Utilities.MessageBoxButton.OK);
                    return;
                }
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection);

                cmd.CommandTimeout = 600;

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    stores.Add(new Store((string)row.ItemArray[0], (string)row.ItemArray[1]));
                }

                // Bind to the combobox
                cboxSelectedStore.ItemsSource = stores;
                cboxSelectedStore.DisplayMemberPath = "Description";
                cboxSelectedStore.SelectedValuePath = "Code";
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", Utilities.MessageBoxButton.OK);
                return;
            }
        }
        private void SetDefaultStore()
        {
            if (stores.Count == 0)
                return;

            Dictionary<string, string> codePrefixList = new Dictionary<string, string>();
            codePrefixList.Add("ACB", "403");
            codePrefixList.Add("ACD", "401");
            codePrefixList.Add("ALM", "631");
            codePrefixList.Add("AST", "601");
            codePrefixList.Add("ATC", "201");
            codePrefixList.Add("BAL", "454");
            codePrefixList.Add("MOB", "211");
            codePrefixList.Add("BRQ", "391");
            codePrefixList.Add("BOS", "425");
            codePrefixList.Add("BUF", "105");
            codePrefixList.Add("CNV", "383");
            codePrefixList.Add("CCD", "431");
            codePrefixList.Add("CCM", "432");
            codePrefixList.Add("CMY", "441");
            codePrefixList.Add("CEN", "459");
            codePrefixList.Add("DSE", "903");
            codePrefixList.Add("EDC", "902");
            codePrefixList.Add("CHA", "371");
            codePrefixList.Add("CHE", "901");
            codePrefixList.Add("CLR", "321");
            codePrefixList.Add("CLM", "103");
            codePrefixList.Add("COO", "621");
            codePrefixList.Add("COR", "313");
            codePrefixList.Add("DET", "109");
            codePrefixList.Add("ECY", "457");
            codePrefixList.Add("ELL", "233");
            codePrefixList.Add("CLF", "104");
            codePrefixList.Add("FIN", "463");
            codePrefixList.Add("FMX", "461");
            codePrefixList.Add("FMY", "311");
            codePrefixList.Add("FWW", "446");
            codePrefixList.Add("GAL", "231");
            codePrefixList.Add("GRH", "112");
            codePrefixList.Add("HNO", "662");
            codePrefixList.Add("HOU", "232");
            codePrefixList.Add("JUP", "351");
            codePrefixList.Add("KET", "511");
            codePrefixList.Add("KOD", "501");
            codePrefixList.Add("MAY", "381");
            codePrefixList.Add("BMB", "341");
            codePrefixList.Add("MIC", "222");
            codePrefixList.Add("MIL", "114");
            codePrefixList.Add("NHV", "408");
            codePrefixList.Add("OPA", "331");
            codePrefixList.Add("CLV", "101");
            codePrefixList.Add("PET", "651");
            codePrefixList.Add("PHI", "448");
            codePrefixList.Add("PTA", "612");
            codePrefixList.Add("PTL", "603");
            codePrefixList.Add("PTS", "462");
            codePrefixList.Add("RCK", "423");
            codePrefixList.Add("SDG", "632");
            codePrefixList.Add("SJU", "397");
            codePrefixList.Add("SPD", "634");
            codePrefixList.Add("SDK", "312");
            codePrefixList.Add("SSM", "110");
            codePrefixList.Add("SEA", "611");
            codePrefixList.Add("SPT", "421");
            codePrefixList.Add("STE", "456");
            codePrefixList.Add("STP", "301");
            codePrefixList.Add("SWH", "424");
            codePrefixList.Add("TOL", "107");
            codePrefixList.Add("TRV", "111");
            codePrefixList.Add("WES", "411");
            codePrefixList.Add("YRK", "451");
            codePrefixList.Add("YRM", "452");
            codePrefixList.Add("ITL", "901");
            codePrefixList.Add("TRA", "901");

            string machinePrefix = Environment.MachineName.Substring(0, 3).ToUpper();

            if (codePrefixList.ContainsKey(machinePrefix))
            {
                cboxSelectedStore.SelectedValue = codePrefixList[machinePrefix];
                session.LocationCode = codePrefixList[machinePrefix];
            }
            else
            {
                cboxSelectedStore.SelectedIndex = 0;
            }
        }

        private void BtnVMIWeeklyReport_Click(object sender, RoutedEventArgs e)
        {
            if (cboxSelectedStore.SelectedIndex == -1)
            {
                MBWindow.Show("Please select a store.", "Error", Utilities.MessageBoxButton.OK);
                return;
            }

            VMIWeeklyStatusClassification status;
            if (rbVMIWeeklyInProgress.IsChecked == true)
                status = VMIWeeklyStatusClassification.InProgress;
            else
            {
                if (rbVMIWeeklyNew.IsChecked == true)
                    status = VMIWeeklyStatusClassification.New;
                else
                {
                    status = VMIWeeklyStatusClassification.All;
                }
            }   
            VMIWeeklyReport vmiWeeklyReport = new VMIWeeklyReport(cboxSelectedStore.SelectedValue.ToString(), dpVMICreateDateFrom.SelectedDate.Value, dpVMICreateDateTo.SelectedDate.Value, status);
            ReportGenerationWindow reportGenerationWindow = new ReportGenerationWindow(vmiWeeklyReport);
            reportGenerationWindow.ShowDialog();
        }

        private void BtnPanelCollapser_Click(object sender, RoutedEventArgs e)
        {
            Button buttonClicked = (Button)sender;
            StackPanel panelBeingCollapsed;
            
            switch (buttonClicked.Name)
            {
                case ("btnPurchaseOrderCollapser"):
                    panelBeingCollapsed = pnlPurchaseOrderCollapsable;
                    session.PurchaseOrderReportShown = !session.PurchaseOrderReportShown;
                    break;
                case ("btnVMICollapser"):
                    panelBeingCollapsed = pnlVMICollapsable;
                    session.VMIRegisterReportShown = !session.VMIRegisterReportShown;
                    break;
                case ("btnWeeklyVMICollapser"):
                    panelBeingCollapsed = pnlWeeklyVMICollapsable;
                    session.VMIWeeklyReportShown = !session.VMIWeeklyReportShown;
                    break;
                case ("btnTransferReceiveCollapser"):
                    panelBeingCollapsed = pnlTransferReceiveCollapsable;
                    session.TransferReceiptReportShown = !session.TransferReceiptReportShown;
                    break;
                case ("btnTransferShipCollapser"):
                    panelBeingCollapsed = pnlTransferShipCollapsable;
                    session.TransferShipReportShown = !session.TransferShipReportShown;
                    break;
                case ("btnRTVCollapser"):
                    panelBeingCollapsed = pnlRTVCollapsable;
                    session.RTVReportShown = !session.RTVReportShown;
                    break;
                default:
                    return;
            }

            if (panelBeingCollapsed.Visibility == Visibility.Collapsed)
            {
                panelBeingCollapsed.Visibility = Visibility.Visible;                
            }
            else
            {
                panelBeingCollapsed.Visibility = Visibility.Collapsed;                
            }
            this.Height = session.Height;
        }

        private void CboxSelectedStore_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(cboxSelectedStore.SelectedValue == null))
                session.LocationCode = cboxSelectedStore.SelectedValue.ToString();
        }
    }
}
