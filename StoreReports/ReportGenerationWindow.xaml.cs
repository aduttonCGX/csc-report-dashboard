﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using StoreReports.Reporting;
using StoreReports.Utilities;

namespace StoreReports
{
    /// <summary>
    /// Interaction logic for ReportGenerationWindow.xaml
    /// </summary>
    public partial class ReportGenerationWindow : Window
    {
        private Report report;

        public ReportGenerationWindow(Report report)
        {
            InitializeComponent();
            this.report = report;            
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            try
            {
                report.QueryData();
            }
            catch (Exception ex)
            {
                MBWindow.Show($"Report query failed: {ex.Message}", "Error", Utilities.MessageBoxButton.OK);
                Logger.LogError(ex);
                return;
            }
            try
            {
                report.GenerateExcelFile();
            }
            catch (Exception ex)
            {
                MBWindow.Show($"Report generation failed: {ex.Message}", "Error", Utilities.MessageBoxButton.OK);
                Logger.LogError(ex);
                return;
            }            
            Close();
        }
    }
}
