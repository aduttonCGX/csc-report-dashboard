﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoreReports.Utilities;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.Diagnostics;

namespace StoreReports.Reporting
{    
    public class TransferShipReport : Report
    {
        private const string REPORT_TITLE = "Transfer Ship Report";

        private List<TransferShip> transferShips;
        private string storeCode;
        private DateTime createDateFrom;
        private DateTime createDateTo;
        public List<TransferShip> TransferShips { get => transferShips; }
        public string StoreCode { get => storeCode; set => storeCode = value; }
        public DateTime CreateDateFrom { get => createDateFrom; set => createDateFrom = value; }
        public DateTime CreateDateTo { get => createDateTo; set => createDateTo = value; }

        public TransferShipReport(string storeCode, DateTime createDateFrom, DateTime createDateTo)
        {
            this.storeCode = storeCode;
            this.createDateFrom = createDateFrom;
            this.createDateTo = new DateTime(createDateTo.Year, createDateTo.Month, createDateTo.Day, 23, 59, 59);
        }

        public override void QueryData()
        {
            transferShips = new List<TransferShip>();

            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    string storeClause = (storeCode == "ALL") ? "" : $@"locationFrom=""{storeCode}"" ";
                    string criteriaXML = $@"<criteria {storeClause}createDateFrom=""{createDateFrom.ToString("yyyy-MM-dd")}"" createDateTo=""{createDateTo.ToString("yyyy-MM-dd")}"" isRTV=""0"" sourceLocType=""0"" enforceUserRightsLocationFrom=""true"" enforceUserRightsLocationTo=""false"" setByFilter=""true""/>";

                    SqlCommand sqlcommand = new SqlCommand("ar_sp_ShipmentGetByCriteria", conn);
                    sqlcommand.CommandType = CommandType.StoredProcedure;
                    sqlcommand.Parameters.Add(new SqlParameter("@i_JustCount", 0));
                    sqlcommand.Parameters.Add(new SqlParameter("@i_criteriaXml", SqlDbType.Xml) { Value = criteriaXML });
                    sqlcommand.Parameters.Add(new SqlParameter("@i_UserCode", "mgb"));
                    SqlDataReader reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        TransferShip transferShip = new TransferShip();
                        transferShip.Transfer = (reader["code"] == DBNull.Value) ? "" : reader["code"].ToString();
                        transferShip.SourceLocation = (reader["locationFromCodeAndDescription"] == DBNull.Value) ? "" : reader["locationFromCodeAndDescription"].ToString();
                        transferShip.TargetLocation = (reader["locationToCodeAndDescription"] == DBNull.Value) ? "" : reader["locationToCodeAndDescription"].ToString();
                        transferShip.Status = (reader["destStatusDescription"] == DBNull.Value) ? "" : reader["destStatusDescription"].ToString();
                        transferShip.Carrier = (reader["carrierDescription"] == DBNull.Value) ? "" : reader["carrierDescription"].ToString();
                        transferShip.Description = (reader["description"] == DBNull.Value) ? "" : reader["description"].ToString();
                        transferShip.CreateDate = (DateTime)reader["createDate"];
                        transferShips.Add(transferShip);
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database read failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        public override void GenerateExcelFile()
        {
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Archive\\" + DateTime.Now.ToString(@"yyyy\\MM\\dd");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string fileName = $"{REPORT_TITLE.Replace(" ", string.Empty)}_{storeCode}_{createDateFrom.ToString("yyyyMMdd")}-{createDateTo.ToString("yyyyMMdd")}_000.xlsx";

            // Make sure you're writing to a new file
            Directory.CreateDirectory(directory);
            int fileNumber = 0;
            while (File.Exists(Path.Combine(directory, fileName)))
            {
                fileNumber++;
                fileName = $"{REPORT_TITLE.Replace(" ", string.Empty)}_{storeCode}_{createDateFrom.ToString("yyyyMMdd")}-{createDateTo.ToString("yyyyMMdd")}_{fileNumber.ToString("D" + 3)}.xlsx";
            }
            
            try
            {
                // Start Excel
                Application excel = new Application();
                excel.Visible = false;
                excel.DisplayAlerts = false;
                Workbook workbook = excel.Workbooks.Add(Type.Missing);
                Worksheet worksheet = workbook.ActiveSheet;
                worksheet.Name = REPORT_TITLE.Replace(" ", string.Empty);
                Range formatRange;

                //  Margins 
                worksheet.PageSetup.Orientation = XlPageOrientation.xlLandscape;
                worksheet.PageSetup.TopMargin = excel.InchesToPoints(0.75);
                worksheet.PageSetup.BottomMargin = excel.InchesToPoints(0.75);
                worksheet.PageSetup.LeftMargin = excel.InchesToPoints(0.25);
                worksheet.PageSetup.RightMargin = excel.InchesToPoints(0.25);
                worksheet.PageSetup.CenterHorizontally = true;

                // Column widths
                worksheet.Columns["A:A"].ColumnWidth = 7;
                worksheet.Columns["B:B"].ColumnWidth = 31;
                worksheet.Columns["C:C"].ColumnWidth = 32;
                worksheet.Columns["D:D"].ColumnWidth = 15;
                worksheet.Columns["E:E"].ColumnWidth = 6;
                worksheet.Columns["F:F"].ColumnWidth = 18;
                worksheet.Columns["G:G"].ColumnWidth = 18;

                // Report Header
                Range headerRange = (Range)worksheet.Range[worksheet.Cells[1, 1], worksheet.Cells[1, 7]];
                headerRange.Merge();
                headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                headerRange.Font.Size = 16;
                headerRange.Font.Bold = true;
                headerRange.Font.Color = ColorTranslator.ToOle(Color.White);
                headerRange.Interior.Color = ColorTranslator.ToOle(Color.Gray);
                headerRange.RowHeight = 36;
                worksheet.Cells[1, 1] = REPORT_TITLE;

                // Store and Date
                worksheet.Cells[2, 2] = "Store Number:";
                worksheet.Cells[2, 3] = storeCode;
                worksheet.Cells[3, 2] = "Create Date From:";
                worksheet.Cells[3, 3] = createDateFrom.ToString("M/d/yyyy");
                worksheet.Cells[4, 2] = "Create Date To:";
                worksheet.Cells[4, 3] = createDateTo.ToString("M/d/yyyy");
                formatRange = (Range)worksheet.Range[worksheet.Cells[2, 2], worksheet.Cells[4, 2]];
                formatRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                formatRange.Font.Bold = true;
                formatRange = (Range)worksheet.Range[worksheet.Cells[2, 3], worksheet.Cells[4, 3]];
                formatRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;

                // Data Column Header
                Range dataColumnHeaderRange = (Range)worksheet.Range[worksheet.Cells[6, 1], worksheet.Cells[6, 7]];
                dataColumnHeaderRange.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                dataColumnHeaderRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                dataColumnHeaderRange.Font.Size = 10;
                dataColumnHeaderRange.Font.Bold = true;
                worksheet.Cells[6, 1] = "Transfer";
                worksheet.Cells[6, 2] = "Source Location";
                worksheet.Cells[6, 3] = "Target Location";
                worksheet.Cells[6, 4] = "Status";
                worksheet.Cells[6, 5] = "Carrier";
                worksheet.Cells[6, 6] = "Description";
                worksheet.Cells[6, 7] = "Create Date";

                // Data
                int row = 7;
                foreach (TransferShip transferShip in TransferShips.OrderBy(t => t.SourceLocation).ThenBy(t => t.Transfer))
                {
                    Range dataLineRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 7]];
                    worksheet.Cells[row, 1].NumberFormat = "@";
                    worksheet.Cells[row, 1] = transferShip.Transfer;
                    worksheet.Cells[row, 2] = transferShip.SourceLocation;
                    worksheet.Cells[row, 3] = transferShip.TargetLocation;
                    worksheet.Cells[row, 4] = transferShip.Status;
                    worksheet.Cells[row, 5] = transferShip.Carrier;
                    worksheet.Cells[row, 6] = transferShip.Description;
                    worksheet.Cells[row, 7] = transferShip.CreateDate.Value.ToString("MM/dd/yyyy hh:mm tt");
                    if (row % 2 == 0)
                    {
                        dataLineRange.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(230, 230, 230));
                    }
                    row++;
                }
                Range dataRange = (Range)worksheet.Range[worksheet.Cells[7, 1], worksheet.Cells[row, 7]];
                dataRange.Font.Size = 9;
                dataRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                dataRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                dataRange.WrapText = true;
                Range createDateRange = (Range)worksheet.Range[worksheet.Cells[7, 7], worksheet.Cells[row, 7]];
                createDateRange.NumberFormat = "mm/dd/yyyy h:mm AM/PM";

                // Page Numbering
                worksheet.PageSetup.LeftFooter = REPORT_TITLE;
                worksheet.PageSetup.CenterFooter = "&P/&N";
                worksheet.PageSetup.RightFooter = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                // Save and close
                workbook.SaveAs(Path.Combine(directory, fileName));
                workbook.Close();
                excel.Quit();
            }
            catch (Exception ex)
            {
                MBWindow.Show($"Report generation failed: {ex.Message}", "Error", Utilities.MessageBoxButton.OK);
                Logger.LogError(ex);
                return;
            }            

            // Open the folder
            bool explorerOpen = false;
            Process[] processes = Process.GetProcessesByName("explorer");
            foreach (Process process in processes)
            {
                if (process.MainWindowTitle == DateTime.Now.ToString(@"dd"))
                {
                    explorerOpen = true;
                    break;
                }
            }
            if (!explorerOpen)
                Process.Start("explorer.exe", directory);
        }
    }
}
