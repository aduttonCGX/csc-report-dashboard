﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoreReports.Utilities;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.Diagnostics;

namespace StoreReports.Reporting
{
    public class PurchaseOrderReport : Report
    {
        private const string REPORT_TITLE = "Purchase Order Report";
        private List<PurchaseOrder> purchaseOrders;
        private string storeCode;
        private DateTime receiptDateFrom;
        private DateTime receiptDateTo;

        public List<PurchaseOrder> PurchaseOrders { get => purchaseOrders; }
        public string StoreCode { get => storeCode; set => storeCode = value; }
        public DateTime ReceiptDateFrom { get => receiptDateFrom; set => receiptDateFrom = value; }
        public DateTime ReceiptDateTo { get => receiptDateTo; set => receiptDateTo = value; }

        public PurchaseOrderReport(string storeCode, DateTime receiptDateFrom, DateTime receiptDateTo)
        {
            this.storeCode = storeCode;
            this.receiptDateFrom = receiptDateFrom;
            this.receiptDateTo = new DateTime(receiptDateTo.Year, receiptDateTo.Month, receiptDateTo.Day, 23, 59, 59);
        }
        public override void QueryData()
        {
            List<PurchaseOrder> unfilteredPurchaseOrders = new List<PurchaseOrder>();
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    string storeClause = (storeCode == "ALL") ? "" : $@"LocationCode=""{storeCode}"" ";
                    string criteriaXML1 = $@"<Criteria BreakdownToLocations=""1"" {storeClause}LocationType=""0"" Mode=""0"" ReceiptDateFrom=""{receiptDateFrom.ToString("yyyy-MM-dd")}"" ReceiptDateTo=""{receiptDateTo.ToString("yyyy-MM-dd")}"" SetByFilter=""true"" Statuses=""0,6,7,9""> <Statuses> <Status Code=""0""/> <Status Code=""6""/> <Status Code=""9""/> </Statuses> <OrderTypes/> <AllocStatuses/> </Criteria>";

                    SqlCommand sqlcommand = new SqlCommand("ar_sp_PurchaseOrderGetByCriteria", conn);
                    sqlcommand.CommandType = CommandType.StoredProcedure;
                    sqlcommand.Parameters.Add(new SqlParameter("@i_TraceOn", 1));
                    sqlcommand.Parameters.Add(new SqlParameter("@i_JustCount", 0));
                    sqlcommand.Parameters.Add(new SqlParameter("@i_UserCode", "mgb"));
                    sqlcommand.Parameters.Add(new SqlParameter("@i_criteriaXml", SqlDbType.Xml) { Value = criteriaXML1 });
                    SqlDataReader reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        PurchaseOrder purchaseOrder = new PurchaseOrder();
                        purchaseOrder.Code = reader["code"].ToString();
                        purchaseOrder.VendorCodeAndDescription = reader["vendorCodeAndDescription"].ToString();
                        purchaseOrder.LocationCodeAndDescription = reader["locationCodeAndDescription"].ToString();
                        purchaseOrder.PoStatus = int.Parse(reader["status"].ToString());
                        purchaseOrder.TypeDescription = reader["typeDescription"].ToString();
                        purchaseOrder.BuyerCode = reader["buyerCode"].ToString();
                        purchaseOrder.BuyerCodeAndDescription = reader["buyerCodeAndDescription"].ToString();
                        purchaseOrder.DateLastReceived = (DateTime)reader["lastReceiptDate"];
                        unfilteredPurchaseOrders.Add(purchaseOrder);
                    }
                    reader.Close();

                    string criteriaXML2 = $@"<Criteria BreakdownToLocations=""1"" {storeClause}LocationType=""0"" Mode=""0"" ReceiptDateFrom=""{receiptDateFrom.ToString("yyyy-MM-dd")}"" ReceiptDateTo=""{receiptDateTo.ToString("yyyy-MM-dd")}"" SetByFilter=""true"" Statuses=""0,6,7,9""> <Statuses> <Status Code=""7""/> </Statuses> <OrderTypes/> <AllocStatuses/> </Criteria>";

                    sqlcommand = new SqlCommand("ar_sp_PurchaseOrderGetByCriteria", conn);
                    sqlcommand.CommandType = CommandType.StoredProcedure;
                    sqlcommand.Parameters.Add(new SqlParameter("@i_TraceOn", 1));
                    sqlcommand.Parameters.Add(new SqlParameter("@i_JustCount", 0));
                    sqlcommand.Parameters.Add(new SqlParameter("@i_UserCode", "mgb"));
                    sqlcommand.Parameters.Add(new SqlParameter("@i_criteriaXml", SqlDbType.Xml) { Value = criteriaXML2 });
                    reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        PurchaseOrder purchaseOrder = new PurchaseOrder();
                        purchaseOrder.Code = reader["code"].ToString();
                        purchaseOrder.VendorCodeAndDescription = reader["vendorCodeAndDescription"].ToString();
                        purchaseOrder.LocationCodeAndDescription = reader["locationCodeAndDescription"].ToString();
                        purchaseOrder.PoStatus = int.Parse(reader["status"].ToString());
                        purchaseOrder.TypeDescription = reader["typeDescription"].ToString();
                        purchaseOrder.BuyerCode = reader["buyerCode"].ToString();
                        purchaseOrder.BuyerCodeAndDescription = reader["buyerCodeAndDescription"].ToString();
                        purchaseOrder.DateLastReceived = (DateTime)reader["lastReceiptDate"];
                        unfilteredPurchaseOrders.Add(purchaseOrder);
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"[{System.Reflection.MethodBase.GetCurrentMethod().Name}] Database read failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }

            // Filter out VMI POs
            purchaseOrders = new List<PurchaseOrder>();
            foreach (PurchaseOrder po in unfilteredPurchaseOrders)
            {
                if (po.IsTruePO())
                {
                    purchaseOrders.Add(po);
                }
            }
        }
        public override void GenerateExcelFile()
        {
            int reportColumns = 7;
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Archive\\" + DateTime.Now.ToString(@"yyyy\\MM\\dd");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string dateRange = (receiptDateFrom.Date == receiptDateTo.Date) ? receiptDateFrom.ToString("yyyyMMdd") : $"{receiptDateFrom.ToString("yyyyMMdd")}-{receiptDateTo.ToString("yyyyMMdd")}";
            string fileName = $"{REPORT_TITLE.Replace(" ", string.Empty)}_{storeCode}_{dateRange}_000.xlsx";

            // Make sure you're writing to a new file
            Directory.CreateDirectory(directory);
            int fileNumber = 0;
            while (File.Exists(Path.Combine(directory, fileName)))
            {
                fileNumber++;
                fileName = $"{REPORT_TITLE.Replace(" ", string.Empty)}_{storeCode}_{dateRange}_{fileNumber.ToString("D" + 3)}.xlsx";
            }

            try
            {
                // Start Excel
                Application excel = new Application();
                excel.Visible = false;
                excel.DisplayAlerts = false;
                Workbook workbook = excel.Workbooks.Add(Type.Missing);
                Worksheet worksheet = workbook.ActiveSheet;
                worksheet.Name = REPORT_TITLE.Replace(" ", string.Empty);
                Range formatRange;

                //  Margins 
                worksheet.PageSetup.Orientation = XlPageOrientation.xlLandscape;
                worksheet.PageSetup.FitToPagesWide = 1;
                worksheet.PageSetup.TopMargin = excel.InchesToPoints(0.75);
                worksheet.PageSetup.BottomMargin = excel.InchesToPoints(0.75);
                worksheet.PageSetup.LeftMargin = excel.InchesToPoints(0.25);
                worksheet.PageSetup.RightMargin = excel.InchesToPoints(0.25);
                worksheet.PageSetup.CenterHorizontally = true;

                // Column widths
                worksheet.Columns["A:A"].ColumnWidth = 15;
                worksheet.Columns["B:B"].ColumnWidth = 22;
                worksheet.Columns["C:C"].ColumnWidth = 22;
                worksheet.Columns["D:D"].ColumnWidth = 15;
                worksheet.Columns["E:E"].ColumnWidth = 13;
                worksheet.Columns["F:F"].ColumnWidth = 25;
                worksheet.Columns["G:G"].ColumnWidth = 15;

                // Report Header
                Range headerRange = (Range)worksheet.Range[worksheet.Cells[1, 1], worksheet.Cells[1, reportColumns]];
                headerRange.Merge();
                headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                headerRange.Font.Size = 16;
                headerRange.Font.Bold = true;
                headerRange.Font.Color = ColorTranslator.ToOle(Color.White);
                headerRange.Interior.Color = ColorTranslator.ToOle(Color.Gray);
                headerRange.RowHeight = 36;
                worksheet.Cells[1, 1] = REPORT_TITLE;

                // Store and Dates
                worksheet.Cells[2, 2] = "Store Number:";
                worksheet.Cells[2, 3] = storeCode;
                worksheet.Cells[3, 2] = "Receipt Date From:";
                worksheet.Cells[3, 3] = ReceiptDateFrom.ToString("M/d/yyyy");
                worksheet.Cells[4, 2] = "Receipt Date To:";
                worksheet.Cells[4, 3] = ReceiptDateTo.ToString("M/d/yyyy");
                formatRange = (Range)worksheet.Range[worksheet.Cells[2, 2], worksheet.Cells[4, 2]];
                formatRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                formatRange.Font.Bold = true;
                formatRange = (Range)worksheet.Range[worksheet.Cells[2, 3], worksheet.Cells[4, 3]];
                formatRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;

                // Data Column Header
                Range dataColumnHeaderRange = (Range)worksheet.Range[worksheet.Cells[6, 1], worksheet.Cells[6, reportColumns]];
                dataColumnHeaderRange.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                dataColumnHeaderRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                dataColumnHeaderRange.Font.Size = 10;
                dataColumnHeaderRange.Font.Bold = true;
                worksheet.Cells[6, 1] = "Purchase Order";
                worksheet.Cells[6, 2] = "From Vendor";
                worksheet.Cells[6, 3] = "To Location";
                worksheet.Cells[6, 4] = "Status";
                worksheet.Cells[6, 5] = "Type";
                worksheet.Cells[6, 6] = "Buyer";
                worksheet.Cells[6, 7] = "Date Last Received";

                // Data
                int row = 7;
                                
                foreach (PurchaseOrder purchaseOrder in PurchaseOrders.OrderBy(p => p.LocationCodeAndDescription).ThenBy(p => p.Code))
                {
                    Range dataLineRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, reportColumns]];
                    worksheet.Cells[row, 1].NumberFormat = "@";
                    worksheet.Cells[row, 1] = purchaseOrder.Code;
                    worksheet.Cells[row, 2] = purchaseOrder.VendorCodeAndDescription;
                    worksheet.Cells[row, 3] = purchaseOrder.LocationCodeAndDescription;
                    worksheet.Cells[row, 4] = purchaseOrder.Status;
                    worksheet.Cells[row, 5] = purchaseOrder.TypeDescription;
                    worksheet.Cells[row, 6] = purchaseOrder.BuyerCodeAndDescription;
                    worksheet.Cells[row, 7] = purchaseOrder.DateLastReceived.ToString("M/d/yyyy");
                    if (row % 2 == 1)
                    {
                        dataLineRange.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(230, 230, 230));
                    }
                    row++;
                }
                Range dataRange = (Range)worksheet.Range[worksheet.Cells[6, 1], worksheet.Cells[row, reportColumns]];
                dataRange.Font.Size = 9;
                dataRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                dataRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                dataRange.WrapText = true;

                // Page Numbering
                worksheet.PageSetup.LeftFooter = REPORT_TITLE;
                worksheet.PageSetup.CenterFooter = "&P/&N";
                worksheet.PageSetup.RightFooter = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                // Save and close
                workbook.SaveAs(Path.Combine(directory, fileName));
                workbook.Close();
                excel.Quit();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"[{System.Reflection.MethodBase.GetCurrentMethod().Name}] Report generation failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }

            // Open the folder
            bool explorerOpen = false;
            Process[] processes = Process.GetProcessesByName("explorer");
            foreach (Process process in processes)
            {
                if (process.MainWindowTitle == DateTime.Now.ToString(@"dd"))
                {
                    explorerOpen = true;
                    break;
                }
            }
            if (!explorerOpen)
                Process.Start("explorer.exe", directory);
        }        
    }
}
