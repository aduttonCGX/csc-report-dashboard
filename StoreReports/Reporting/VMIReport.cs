﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoreReports.Utilities;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.Diagnostics;

namespace StoreReports.Reporting
{
    public class VMIReport : Report
    {
        #region Private Members
        private const string REPORT_TITLE = "VMI Register Report";
        private List<VMI> vmis = new List<VMI>();
        private string storeCode;
        private DateTime closedDate;
        #endregion

        #region Public Properties
        public List<VMI> VMIs { get => vmis; }
        public string StoreCode { get => storeCode; set => storeCode = value; }
        public DateTime ClosedDate { get => closedDate; set => closedDate = value; }
        #endregion

        #region Constructors
        public VMIReport(string storeCode, DateTime closedDate)
        {
            this.storeCode = storeCode;
            this.closedDate = closedDate;
            vmis = new List<VMI>();
        }
        public VMIReport()
        {
            storeCode = "";
            closedDate = DateTime.MinValue;
            vmis = new List<VMI>();
        }
        #endregion
        
        public override void QueryData()
        {
            // Get VMIs
            vmis = new List<VMI>();
            try
            {
                using (SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString))
                {
                    connection.Open();
                    string storeClause = (storeCode == "ALL") ? "" : $@"StoreCode = '{storeCode}' AND ";
                    string query = $@"SELECT 
	                                    [StoreCodeAndDescription] AS 'StoreDescription',
	                                    [VendorCodeAndDescription] AS 'VendorDescription',
	                                    [ID] AS 'VMINumber',
	                                    'Closed' AS 'Status',
	                                    [BusinessDate] AS 'TransactionDate',
	                                    [VendorRef] AS 'InvoiceNumber',
	                                    CAST([TotalCost] AS NUMERIC(36,2)) AS 'InvoiceTotal',
                                        RTVCode
                                    FROM [mi9_mms_cg_live].[dbo].[v_VMITransactionsForWeb]
                                    WHERE 
	                                    {storeClause}
	                                    DateClosed >= '{closedDate.ToString("yyyy-MM-dd")}' AND DateClosed < DATEADD(DAY,1,'{closedDate.ToString("yyyy-MM-dd")}')
	                                    AND [Status] = 7";

                    SqlCommand sqlcommand = new SqlCommand(query, connection);
                    sqlcommand.CommandTimeout = 1200;
                    SqlDataReader reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        VMI vmi = new VMI();
                        vmi.StoreDescription = (reader["StoreDescription"] == DBNull.Value) ? "" : reader["StoreDescription"].ToString();
                        vmi.VendorDescription = (reader["VendorDescription"] == DBNull.Value) ? "" : reader["VendorDescription"].ToString();
                        vmi.VMINumber = (reader["VMINumber"] == DBNull.Value) ? "" : reader["VMINumber"].ToString();
                        vmi.Status = (reader["Status"] == DBNull.Value) ? "" : reader["Status"].ToString();
                        vmi.TransactionDate = (DateTime)reader["TransactionDate"];
                        vmi.InvoiceNumber = (reader["InvoiceNumber"] == DBNull.Value) ? "" : reader["InvoiceNumber"].ToString();
                        vmi.InvoiceTotal = (reader["InvoiceTotal"] == DBNull.Value) ? 0.00f : double.Parse(reader["InvoiceTotal"].ToString());
                        vmi.RTVCode = (reader["RTVCode"] == DBNull.Value) ? "" : reader["RTVCode"].ToString();
                        vmi.IsRTV = string.IsNullOrWhiteSpace(vmi.RTVCode) ? false : true;                        
                        vmis.Add(vmi);
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database read failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }

            // Get VMI line items
            try
            {
                foreach (VMI vmi in vmis)
                {
                    using (SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString))
                    {
                        connection.Open();
                        string query = $@"SELECT DISTINCT
	                                        vt.LineNumber AS 'LineNumber',
	                                        dl.deliveryno AS 'ReceivingMemo',
	                                        vt.VendorProductCode AS 'VendorStockNumber',
	                                        vt.ProductDescription AS 'Description',
	                                        vt.BaseCode AS 'SKU',
	                                        vt.qty AS 'Quantity',
	                                        CAST(COALESCE(pol.UnitCost, cl.avcost) AS NUMERIC(36,2)) AS 'SystemCost',
	                                        CAST(vt.InvoiceCost AS NUMERIC(36,2)) AS 'InvoiceCost',
	                                        CAST(vt.ExtInvoiceCost AS NUMERIC(36,2)) AS 'ExtendedInvoiceCost',
	                                        po.Code AS 'PONumber'	
                                        FROM 
	                                        [mi9_mms_cg_live].[dbo].[v_VMITransactionLinesForWeb] vt
	                                        LEFT JOIN [mi9_mms_cg_live].[dbo].[ar_v_PurchaseOrders] po ON po.VMITransactionID = vt.VMITransactionID
	                                        FULL JOIN [mi9_mms_cg_live].[dbo].[ar_v_PurchaseOrderLines] pol ON vt.POLineID = pol.ID
	                                        LEFT JOIN [mi9_mms_cg_live].[dbo].[DeliveryLine] dl ON pol.ID = dl.BasePOLineID
	                                        FULL JOIN [mi9_mms_cg_live].[dbo].[consiglines] cl ON vt.RTVLineID = cl.conlineint
                                        WHERE
	                                        vt.VMITransactionID = {vmi.VMINumber}
                                            AND (po.ID = pol.[PurchaseOrderID] OR pol.[PurchaseOrderID] IS NULL)
                                        ORDER BY 
	                                        vt.LineNumber";

                        SqlCommand sqlcommand = new SqlCommand(query, connection);
                        sqlcommand.CommandTimeout = 1200;
                        SqlDataReader reader = sqlcommand.ExecuteReader();

                        while (reader.Read())
                        {
                            VMILineItem vmiLineItem = new VMILineItem();
                            vmiLineItem.LineNumber = (reader["LineNumber"] == DBNull.Value) ? 0 : int.Parse(reader["LineNumber"].ToString());
                            vmiLineItem.RTV = (vmiLineItem.LineNumber > 0) ? "" : vmi.RTVCode;
                            vmiLineItem.ReceivingMemo = (reader["ReceivingMemo"] == DBNull.Value) ? "" : reader["ReceivingMemo"].ToString();
                            vmiLineItem.VendorStockNumber = (reader["VendorStockNumber"] == DBNull.Value) ? "" : reader["VendorStockNumber"].ToString();
                            vmiLineItem.Description = (reader["Description"] == DBNull.Value) ? "" : reader["Description"].ToString();
                            vmiLineItem.SKU = (reader["SKU"] == DBNull.Value) ? "" : reader["SKU"].ToString();
                            vmiLineItem.Quantity = (reader["Quantity"] == DBNull.Value) ? 0 : int.Parse(reader["Quantity"].ToString());                            
                            vmiLineItem.SystemCost = (reader["SystemCost"] == DBNull.Value) ? 0.00f : double.Parse(reader["SystemCost"].ToString());
                            vmiLineItem.InvoiceCost = (reader["InvoiceCost"] == DBNull.Value) ? 0.00f : double.Parse(reader["InvoiceCost"].ToString());
                            vmiLineItem.ExtendedInvoiceCost = (reader["ExtendedInvoiceCost"] == DBNull.Value) ? 0.00f : double.Parse(reader["ExtendedInvoiceCost"].ToString());
                            vmiLineItem.PONumber = (reader["PONumber"] == DBNull.Value) ? "" : reader["PONumber"].ToString();
                            vmi.Items.Add(vmiLineItem);
                        }
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database read failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        public override void GenerateExcelFile()
        {
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Archive\\" + DateTime.Now.ToString(@"yyyy\\MM\\dd");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string fileName = $"{REPORT_TITLE.Replace(" ", string.Empty)}_{storeCode}_{closedDate.ToString("yyyyMMdd")}_000.xlsx";

            // Make sure you're writing to a new file
            Directory.CreateDirectory(directory);
            int fileNumber = 0;
            while (File.Exists(Path.Combine(directory, fileName)))
            {
                fileNumber++;
                fileName = $"{REPORT_TITLE.Replace(" ", string.Empty)}_{storeCode}_{closedDate.ToString("yyyyMMdd")}_{fileNumber.ToString("D" + 3)}.xlsx";
            }

            try
            {
                // Start Excel
                Application excel = new Application();
                excel.Visible = false;
                excel.DisplayAlerts = false;
                Workbook workbook = excel.Workbooks.Add(Type.Missing);
                Worksheet worksheet = workbook.ActiveSheet;
                worksheet.Name = REPORT_TITLE.Replace(" ", string.Empty);
                Range formatRange;

                // Margins 
                worksheet.PageSetup.Orientation = XlPageOrientation.xlLandscape;
                worksheet.PageSetup.TopMargin = excel.InchesToPoints(0.75);
                worksheet.PageSetup.BottomMargin = excel.InchesToPoints(0.75);
                worksheet.PageSetup.LeftMargin = excel.InchesToPoints(0.25);
                worksheet.PageSetup.RightMargin = excel.InchesToPoints(0.25);
                worksheet.PageSetup.CenterHorizontally = true;

                // Column widths
                worksheet.Columns["A:A"].ColumnWidth = 9;
                worksheet.Columns["B:B"].ColumnWidth = 19;
                worksheet.Columns["C:C"].ColumnWidth = 32;
                worksheet.Columns["D:D"].ColumnWidth = 15;
                worksheet.Columns["E:E"].ColumnWidth = 10;
                worksheet.Columns["F:F"].ColumnWidth = 9;
                worksheet.Columns["G:G"].ColumnWidth = 9;
                worksheet.Columns["H:H"].ColumnWidth = 12;
                worksheet.Columns["I:I"].ColumnWidth = 11;

                // Report Header
                Range headerRange = (Range)worksheet.Range[worksheet.Cells[1, 1], worksheet.Cells[1, 9]];
                headerRange.Merge();
                headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                headerRange.Font.Size = 16;
                headerRange.Font.Bold = true;
                headerRange.Font.Color = ColorTranslator.ToOle(Color.White);
                headerRange.Interior.Color = ColorTranslator.ToOle(Color.Gray);
                headerRange.RowHeight = 36;
                worksheet.Cells[1, 1] = REPORT_TITLE;

                // Store and Date
                worksheet.Cells[2, 2] = "Store Number:";
                worksheet.Cells[2, 3] = storeCode;
                worksheet.Cells[3, 2] = "Closed Date:";
                worksheet.Cells[3, 3] = closedDate.ToString("M/d/yyyy");
                formatRange = (Range)worksheet.Range[worksheet.Cells[2, 2], worksheet.Cells[3, 2]];
                formatRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                formatRange.Font.Bold = true;
                formatRange = (Range)worksheet.Range[worksheet.Cells[2, 3], worksheet.Cells[3, 3]];
                formatRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;

                int row = 4;
                VMI last = new VMI();
                if (vmis.Count > 0)
                    last = vmis.Last();

                foreach (VMI vmi in vmis.OrderBy(v => v.StoreDescription).ThenBy(v => v.VMINumber))
                {
                    Range dataColumnHeaderRangeLeft, dataColumnHeaderRangeRight, totalsRange;

                    // VMI Main Header Details
                    Range vmiDetailHeaders = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 9]];
                    vmiDetailHeaders.Font.Size = 9;
                    vmiDetailHeaders.Font.Bold = true;
                    vmiDetailHeaders.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                    vmiDetailHeaders.VerticalAlignment = XlVAlign.xlVAlignBottom;
                    vmiDetailHeaders.WrapText = true;
                    worksheet.Cells[row, 1] = "Store";
                    worksheet.Cells[row, 3] = "Vendor";
                    worksheet.Cells[row, 4] = "VMI #";
                    worksheet.Cells[row, 5] = "Status";
                    worksheet.Cells[row, 6] = "Transaction Date";
                    worksheet.Cells[row, 7] = "Invoice";
                    worksheet.Cells[row, 9] = "Invoice Total";
                    worksheet.Cells[row, 9].HorizontalAlignment = XlHAlign.xlHAlignRight;
                    row++;
                    Range vmiDetailData = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 9]];
                    vmiDetailData.Font.Size = 9;
                    vmiDetailData.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                    vmiDetailData.VerticalAlignment = XlVAlign.xlVAlignTop;
                    vmiDetailData.NumberFormat = "@";
                    worksheet.Cells[row, 1] = vmi.StoreDescription;
                    worksheet.Cells[row, 3] = vmi.VendorDescription;
                    worksheet.Cells[row, 4] = vmi.VMINumber;
                    worksheet.Cells[row, 5] = vmi.Status;
                    worksheet.Cells[row, 6].NumberFormat = "M/D/YYYY";
                    worksheet.Cells[row, 6] = vmi.TransactionDate.ToString("M/d/yyyy");
                    worksheet.Cells[row, 7] = vmi.InvoiceNumber;
                    worksheet.Cells[row, 9].NumberFormat = "#,##0.00"; 
                    worksheet.Cells[row, 9] = vmi.InvoiceTotal; 
                    worksheet.Cells[row, 9].HorizontalAlignment = XlHAlign.xlHAlignRight;
                    row++;
                    ((Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 9]]).RowHeight = 7.25;
                    row++;

                    // If RTV, format this way
                    if (vmi.IsRTV)
                    {
                        int topLine = row + 1;
                        // Check to see if there's a PO portion
                        int maxLineNumber = (vmi.Items.Count == 0) ? 0 : vmi.Items.Max(x => x.LineNumber);

                        if (maxLineNumber > 0)
                        {
                            // Data Column Header
                            dataColumnHeaderRangeLeft = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 4]];
                            dataColumnHeaderRangeLeft.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                            dataColumnHeaderRangeLeft.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                            dataColumnHeaderRangeLeft.Font.Size = 10;
                            dataColumnHeaderRangeLeft.Font.Bold = true;
                            dataColumnHeaderRangeLeft.WrapText = true;
                            dataColumnHeaderRangeRight = (Range)worksheet.Range[worksheet.Cells[row, 5], worksheet.Cells[row, 9]];
                            dataColumnHeaderRangeRight.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                            dataColumnHeaderRangeRight.HorizontalAlignment = XlHAlign.xlHAlignRight;
                            dataColumnHeaderRangeRight.Font.Size = 10;
                            dataColumnHeaderRangeRight.Font.Bold = true;
                            dataColumnHeaderRangeRight.WrapText = true;
                            worksheet.Cells[row, 1] = "Receving Memo";
                            worksheet.Cells[row, 2] = "Vendor Stock Number";
                            worksheet.Cells[row, 3] = "Description";
                            worksheet.Cells[row, 4] = "SKU";
                            worksheet.Cells[row, 5] = "Quantity";
                            worksheet.Cells[row, 6] = "System Cost";
                            worksheet.Cells[row, 7] = "Invoice Cost";
                            worksheet.Cells[row, 8] = "Extended Invoice Cost";
                            worksheet.Cells[row, 9] = "PO";
                            row++;

                            // Non-RTV Data Lines
                            List<VMILineItem> nonRTVLineItems = vmi.Items.Where(x => x.LineNumber > 0).OrderBy(x => x.LineNumber).ToList();
                            foreach (VMILineItem item in nonRTVLineItems)
                            {
                                // Data
                                worksheet.Cells[row, 1].NumberFormat = "@";
                                worksheet.Cells[row, 1] = item.ReceivingMemo;
                                worksheet.Cells[row, 2].NumberFormat = "@";
                                worksheet.Cells[row, 2] = item.VendorStockNumber;
                                worksheet.Cells[row, 3].NumberFormat = "@";
                                worksheet.Cells[row, 3] = item.Description;
                                worksheet.Cells[row, 4].NumberFormat = "@";
                                worksheet.Cells[row, 4] = item.SKU;
                                worksheet.Cells[row, 5] = item.Quantity;
                                worksheet.Cells[row, 6].NumberFormat = "#,##0.00";
                                worksheet.Cells[row, 6] = item.SystemCost;
                                worksheet.Cells[row, 7].NumberFormat = "#,##0.00";
                                worksheet.Cells[row, 7] = item.InvoiceCost;
                                worksheet.Cells[row, 8].NumberFormat = "#,##0.00";
                                worksheet.Cells[row, 8] = item.ExtendedInvoiceCost;
                                worksheet.Cells[row, 9].NumberFormat = "@";
                                worksheet.Cells[row, 9] = item.PONumber;

                                // Formatting
                                Range leftDataRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 4]];
                                leftDataRange.Font.Size = 9;
                                leftDataRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                                leftDataRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                                leftDataRange.WrapText = true;
                                Range rightDataRange = (Range)worksheet.Range[worksheet.Cells[row, 5], worksheet.Cells[row, 9]];
                                rightDataRange.Font.Size = 9;
                                rightDataRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                                rightDataRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                                rightDataRange.WrapText = true;
                                if ((row - topLine) % 2 == 1)
                                {
                                    leftDataRange.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(230, 230, 230));
                                    rightDataRange.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(230, 230, 230));
                                }

                                // Iterate
                                row++;
                            }

                            // Totals                            
                            worksheet.Cells[row, 5] = $"=SUM(E{topLine.ToString()}:E{(row - 1).ToString()})";
                            worksheet.Cells[row, 8] = $"=SUM(H{topLine.ToString()}:H{(row - 1).ToString()})";
                            worksheet.Cells[row, 8].NumberFormat = "#,##0.00";
                            totalsRange = (Range)worksheet.Range[worksheet.Cells[row, 5], worksheet.Cells[row, 8]];
                            totalsRange.Font.Size = 9;
                            totalsRange.Font.Bold = true;
                            totalsRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                            totalsRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                            totalsRange.WrapText = true;
                            totalsRange.Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
                            row += 2;
                        }

                        // Print RTV section
                        // Data Column Header
                        topLine = row + 1;
                        dataColumnHeaderRangeLeft = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 4]];
                        dataColumnHeaderRangeLeft.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                        dataColumnHeaderRangeLeft.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                        dataColumnHeaderRangeLeft.Font.Size = 10;
                        dataColumnHeaderRangeLeft.Font.Bold = true;
                        dataColumnHeaderRangeLeft.WrapText = true;
                        dataColumnHeaderRangeRight = (Range)worksheet.Range[worksheet.Cells[row, 5], worksheet.Cells[row, 8]];
                        dataColumnHeaderRangeRight.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                        dataColumnHeaderRangeRight.HorizontalAlignment = XlHAlign.xlHAlignRight;
                        dataColumnHeaderRangeRight.Font.Size = 10;
                        dataColumnHeaderRangeRight.Font.Bold = true;
                        dataColumnHeaderRangeRight.WrapText = true;
                        worksheet.Cells[row, 1] = "RTV";
                        worksheet.Cells[row, 2] = "Vendor Stock Number";
                        worksheet.Cells[row, 3] = "Description";
                        worksheet.Cells[row, 4] = "SKU";
                        worksheet.Cells[row, 5] = "Quantity";
                        worksheet.Cells[row, 6] = "System Cost";
                        worksheet.Cells[row, 7] = "Invoice Cost";
                        worksheet.Cells[row, 8] = "Extended Invoice Cost";
                        row++;

                        // RTV Data Lines
                        List<VMILineItem> rtvLineItems = vmi.Items.Where(x => x.LineNumber < 0).OrderByDescending(x => x.LineNumber).ToList();
                        foreach (VMILineItem item in rtvLineItems)
                        {
                            // Data
                            worksheet.Cells[row, 1].NumberFormat = "@";
                            worksheet.Cells[row, 1] = item.RTV;
                            worksheet.Cells[row, 2].NumberFormat = "@";
                            worksheet.Cells[row, 2] = item.VendorStockNumber;
                            worksheet.Cells[row, 3].NumberFormat = "@";
                            worksheet.Cells[row, 3] = item.Description;
                            worksheet.Cells[row, 4].NumberFormat = "@";
                            worksheet.Cells[row, 4] = item.SKU;
                            worksheet.Cells[row, 5] = item.Quantity;
                            worksheet.Cells[row, 6].NumberFormat = "#,##0.00";
                            worksheet.Cells[row, 6] = item.SystemCost;
                            worksheet.Cells[row, 7].NumberFormat = "#,##0.00";
                            worksheet.Cells[row, 7] = item.InvoiceCost;
                            worksheet.Cells[row, 8].NumberFormat = "#,##0.00";
                            worksheet.Cells[row, 8] = item.ExtendedInvoiceCost;
                            
                            // Formatting
                            Range leftDataRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 4]];
                            leftDataRange.Font.Size = 9;
                            leftDataRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                            leftDataRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                            leftDataRange.WrapText = true;
                            Range rightDataRange = (Range)worksheet.Range[worksheet.Cells[row, 5], worksheet.Cells[row, 8]];
                            rightDataRange.Font.Size = 9;
                            rightDataRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                            rightDataRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                            rightDataRange.WrapText = true;
                            if ((row - topLine) % 2 == 1)
                            {
                                leftDataRange.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(230, 230, 230));
                                rightDataRange.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(230, 230, 230));
                            }

                            // Iterate
                            row++;
                        }

                        // Totals
                        worksheet.Cells[row, 5] = $"=SUM(E{topLine.ToString()}:E{(row - 1).ToString()})";
                        worksheet.Cells[row, 8] = $"=SUM(H{topLine.ToString()}:H{(row - 1).ToString()})";
                        worksheet.Cells[row, 8].NumberFormat = "#,##0.00";
                        totalsRange = (Range)worksheet.Range[worksheet.Cells[row, 5], worksheet.Cells[row, 8]];
                        totalsRange.Font.Size = 9;
                        totalsRange.Font.Bold = true;
                        totalsRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                        totalsRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                        totalsRange.WrapText = true;
                        totalsRange.Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
                        row++;
                    }
                    // Instructions for non-RTV
                    else
                    {
                        // Data Column Header
                        int topLine = row + 1;
                        dataColumnHeaderRangeLeft = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 4]];
                        dataColumnHeaderRangeLeft.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                        dataColumnHeaderRangeLeft.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                        dataColumnHeaderRangeLeft.Font.Size = 10;
                        dataColumnHeaderRangeLeft.Font.Bold = true;
                        dataColumnHeaderRangeLeft.WrapText = true;
                        int width = (vmi.IsRTV) ? 8 : 9;
                        dataColumnHeaderRangeRight = (Range)worksheet.Range[worksheet.Cells[row, 5], worksheet.Cells[row, width]];
                        dataColumnHeaderRangeRight.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                        dataColumnHeaderRangeRight.HorizontalAlignment = XlHAlign.xlHAlignRight;
                        dataColumnHeaderRangeRight.Font.Size = 10;
                        dataColumnHeaderRangeRight.Font.Bold = true;
                        dataColumnHeaderRangeRight.WrapText = true;
                        worksheet.Cells[row, 1] = "Receving Memo";
                        worksheet.Cells[row, 2] = "Vendor Stock Number";
                        worksheet.Cells[row, 3] = "Description";
                        worksheet.Cells[row, 4] = "SKU";
                        worksheet.Cells[row, 5] = "Quantity";
                        worksheet.Cells[row, 6] = "System Cost";
                        worksheet.Cells[row, 7] = "Invoice Cost";
                        worksheet.Cells[row, 8] = "Extended Invoice Cost";
                        worksheet.Cells[row, 9] = "PO";
                        row++;

                        // Data Rows                
                        foreach (VMILineItem item in vmi.Items)
                        {
                            worksheet.Cells[row, 1].NumberFormat = "@";
                            worksheet.Cells[row, 1] = item.ReceivingMemo;
                            worksheet.Cells[row, 2].NumberFormat = "@";
                            worksheet.Cells[row, 2] = item.VendorStockNumber;
                            worksheet.Cells[row, 3].NumberFormat = "@";
                            worksheet.Cells[row, 3] = item.Description;
                            worksheet.Cells[row, 4].NumberFormat = "@";
                            worksheet.Cells[row, 4] = item.SKU;
                            worksheet.Cells[row, 5] = item.Quantity;
                            worksheet.Cells[row, 6].NumberFormat = "#,##0.00";
                            worksheet.Cells[row, 6] = item.SystemCost;
                            worksheet.Cells[row, 7].NumberFormat = "#,##0.00";
                            worksheet.Cells[row, 7] = item.InvoiceCost;
                            worksheet.Cells[row, 8].NumberFormat = "#,##0.00";
                            worksheet.Cells[row, 8] = item.ExtendedInvoiceCost;
                            worksheet.Cells[row, 9].NumberFormat = "@";
                            worksheet.Cells[row, 9] = item.PONumber;

                            // Format
                            Range leftDataRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 4]];
                            leftDataRange.Font.Size = 9;
                            leftDataRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                            leftDataRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                            leftDataRange.WrapText = true;
                            Range rightDataRange = (Range)worksheet.Range[worksheet.Cells[row, 5], worksheet.Cells[row, 9]];
                            rightDataRange.Font.Size = 9;
                            rightDataRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                            rightDataRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                            rightDataRange.WrapText = true;
                            if ((row - topLine) % 2 == 1)
                            {
                                leftDataRange.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(230, 230, 230));
                                rightDataRange.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(230, 230, 230));
                            }

                            // Iterate
                            row++;
                        }

                        // Totals
                        worksheet.Cells[row, 5] = $"=SUM(E{topLine.ToString()}:E{(row - 1).ToString()})";
                        worksheet.Cells[row, 8] = $"=SUM(H{topLine.ToString()}:H{(row - 1).ToString()})";
                        worksheet.Cells[row, 8].NumberFormat = "#,##0.00";
                        totalsRange = (Range)worksheet.Range[worksheet.Cells[row, 5], worksheet.Cells[row, 8]];
                        totalsRange.Font.Size = 9;
                        totalsRange.Font.Bold = true;
                        totalsRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                        totalsRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                        totalsRange.WrapText = true;
                        totalsRange.Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
                        row++;
                    }

                    // Bottom Line
                    if (!vmi.Equals(last))
                    {
                        Range bottomLineRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 9]];
                        bottomLineRange.Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlSlantDashDot;
                        row += 2;
                    }                    
                }

                // Page Numbering
                worksheet.PageSetup.LeftFooter = REPORT_TITLE;
                worksheet.PageSetup.CenterFooter = "&P/&N";
                worksheet.PageSetup.RightFooter = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                // Save and close
                workbook.SaveAs(Path.Combine(directory, fileName));
                workbook.Close();
                excel.Quit();
            }
            catch (Exception ex)
            {
                MBWindow.Show($"Report generation failed: {ex.Message}", "Error", MessageBoxButton.OK);
                Logger.LogError(ex);
                return;
            }

            // Open the folder
            bool explorerOpen = false;
            Process[] processes = Process.GetProcessesByName("explorer");
            foreach (Process process in processes)
            {
                if (process.MainWindowTitle == DateTime.Now.ToString(@"dd"))
                {
                    explorerOpen = true;
                    break;
                }
            }
            if (!explorerOpen)
                Process.Start("explorer.exe", directory);
        }
    }    
}
