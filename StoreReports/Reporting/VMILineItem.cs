﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreReports.Reporting
{
    public class VMILineItem
    {
        #region Private Members
        private string receivingMemo;
        private string vendorStockNumber;
        private string description;
        private string sku;
        private int quantity;
        private double systemCost;
        private double invoiceCost;
        private double extendedInvoiceCost;
        private string poNumber;
        private string rtv;
        private int lineNumber;
        #endregion

        #region Public Properties
        public string ReceivingMemo { get => receivingMemo; set => receivingMemo = value; }
        public string VendorStockNumber { get => vendorStockNumber; set => vendorStockNumber = value; }
        public string Description { get => description; set => description = value; }
        public string SKU { get => sku; set => sku = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public double SystemCost { get => systemCost; set => systemCost = value; }        
        public double InvoiceCost { get => invoiceCost; set => invoiceCost = value; }
        public double ExtendedInvoiceCost { get => extendedInvoiceCost; set => extendedInvoiceCost = value; }
        public string PONumber { get => poNumber; set => poNumber = value; }
        public string RTV { get => rtv; set => rtv = value; }
        public int LineNumber { get => lineNumber; set => lineNumber = value; }
        #endregion

        #region Constructors
        public VMILineItem(string receivingMemo = "", string vendorStockNumber = "", string description = "", string sku = "", int quantity = 0, double systemCost = 0.00f, double invoiceCost = 0.00f, string poNumber = "", string rtv = "", int lineNumber = int.MinValue)
        {
            this.receivingMemo = receivingMemo;
            this.vendorStockNumber = vendorStockNumber;
            this.description = description;
            this.sku = sku;
            this.quantity = quantity;
            this.systemCost = systemCost;
            this.invoiceCost = invoiceCost;
            this.poNumber = poNumber;
            this.rtv = rtv;
            this.lineNumber = lineNumber;
        }
        #endregion


    }
}
