﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoreReports.Utilities;
using System.Data.SqlClient;
using System.Data;


namespace StoreReports.Reporting
{
    public class PurchaseOrder
    {
        #region Private Members
        private string code;
        private string vendorCodeAndDescription;
        private string locationCodeAndDescription;
        private int? poStatus;
        private string typeDescription;
        private string buyerCode;
        private string buyerCodeAndDescription;
        private DateTime dateLastReceived;
        private string webPortalLink;
        #endregion

        #region Public Properties
        public string Code { get => code; set => code = value; }
        public string VendorCodeAndDescription { get => vendorCodeAndDescription; set => vendorCodeAndDescription = value; }
        public string LocationCodeAndDescription { get => locationCodeAndDescription; set => locationCodeAndDescription = value; }
        public int? PoStatus { get => poStatus; set => poStatus = value; }
        public string Status
        {
            get
            {
                if (poStatus.HasValue)
                {
                    switch (poStatus.Value)
                    {
                        case 0:
                            return "Open";
                        case 6:
                            return "Partially Shipped";
                        case 7:
                            return "Completed";
                        case 8:
                            return "Closed";
                        case 9:
                            return "Cancelled";
                        default:
                            return null;
                    }                        
                }
                else
                {
                    return null;
                }
            }
        }
        public string TypeDescription { get => typeDescription; set => typeDescription = value; }
        public string BuyerCode { get => buyerCode; set => buyerCode = value; }
        public string BuyerCodeAndDescription { get => buyerCodeAndDescription; set => buyerCodeAndDescription = value; }
        public DateTime DateLastReceived { get => dateLastReceived; set => dateLastReceived = value; }
        public string WebPortalLink { get => webPortalLink; set => webPortalLink = value; }
        #endregion

        public PurchaseOrder()
        {
            code = null;
            vendorCodeAndDescription = null;
            locationCodeAndDescription = null;
            typeDescription = null;
            buyerCode = null;
            buyerCodeAndDescription = null;
            dateLastReceived = DateTime.MinValue;
            webPortalLink = null;
        }

        public bool IsTruePO()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    string query = $@"SELECT VMITransactionID FROM [mi9_mms_cg_live].[dbo].[ar_v_PurchaseOrders] WHERE Code = '{code}'";
                    SqlCommand sqlcommand = new SqlCommand(query, conn);
                    object result = sqlcommand.ExecuteScalar();

                    // Returns 'false' (PO is a VMI PO) if a VMITransactionID is found.  If not, returns 'true,' indicating that this is a true PO
                    return result == DBNull.Value;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"[{System.Reflection.MethodBase.GetCurrentMethod().Name}] Database read failed on Code {code}. {ex.Message}", "Error", MessageBoxButton.OK);
                return false;
            }
        }

        public void AcquireURL()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    string query = $@"DECLARE
	                                    @PurchaseOrderNumber VARCHAR(8) = '{code}'

                                    SELECT 
	                                    CASE
		                                    WHEN ID IS NOT NULL THEN CONCAT('http://cgescentral/merchant/store/live/purchasing/po/receipt/detail/', ID, '/', LocationCode)
		                                    ELSE ''
	                                    END AS 'Hyperlink'
                                    FROM 
	                                    [mi9_mms_cg_live].[dbo].[ar_v_PurchaseOrders]
                                    WHERE 
	                                    Code = @PurchaseOrderNumber";
                    SqlCommand sqlcommand = new SqlCommand(query, conn);
                    webPortalLink = sqlcommand.ExecuteScalar().ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"[{System.Reflection.MethodBase.GetCurrentMethod().Name}] Database read failed: {ex.Message}", "Error", MessageBoxButton.OK);
            }
        }
    }
}
