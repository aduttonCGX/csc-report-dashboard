﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreReports.Reporting
{
    public enum PriceChangeReportCategory { Permanent, Temporary, All };
    public class PriceChangeReport : Report
    {
        private string store;
        private DateTime startDate;
        private List<PriceChange> temporaryPriceChanges;
        private List<PriceChange> permanentPriceChanges;

        

        public override void QueryData()
        {
            throw new NotImplementedException();
        }
        public override void GenerateExcelFile()
        {
            throw new NotImplementedException();
        }
    }
}
