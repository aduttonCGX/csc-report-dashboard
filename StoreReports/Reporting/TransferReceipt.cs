﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreReports.Reporting
{
    public class TransferReceipt
    {
        private string transfer;
        private string sourceLocation;
        private string targetLocation;
        private string status;
        private string carrier;
        private string description;
        private DateTime? createDate;

        public string Transfer { get => transfer; set => transfer = value; }
        public string SourceLocation { get => sourceLocation; set => sourceLocation = value; }
        public string TargetLocation { get => targetLocation; set => targetLocation = value; }
        public string Status { get => status; set => status = value; }
        public string Carrier { get => carrier; set => carrier = value; }
        public string Description { get => description; set => description = value; }
        public DateTime? CreateDate { get => createDate; set => createDate = value; }

        public TransferReceipt()
        {

        }
    }
}
