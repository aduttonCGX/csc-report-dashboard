﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoreReports.Utilities;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.Diagnostics;

namespace StoreReports.Reporting
{
    public enum RTVReportBasis { ShipDate, CreateDate };
    public class RTVReport : Report
    {
        private const string REPORT_TITLE = "RTVs By Date {0}";
        private List<RTV> rtvs = new List<RTV>();
        private string storeCode;
        private DateTime parameterDateFrom;
        private DateTime parameterDateTo;
        private RTVReportBasis basis;

        public List<RTV> RTVs { get => rtvs; }
        public string StoreCode { get => storeCode; set => storeCode = value; }
        public DateTime ParameterDateFrom { get => parameterDateFrom; set => parameterDateFrom = value; }
        public DateTime ParameterDateTo { get => parameterDateTo; set => parameterDateTo = value; }
        public RTVReportBasis Basis { get => basis; set => basis = value; }

        public RTVReport(string storeCode, DateTime parameterDateFrom, DateTime parameterDateTo, RTVReportBasis basis = RTVReportBasis.ShipDate)
        {
            this.storeCode = storeCode;
            this.parameterDateFrom = parameterDateFrom;
            this.parameterDateTo = new DateTime(parameterDateTo.Year, parameterDateTo.Month, parameterDateTo.Day, 23, 59, 59);
            this.basis = basis;
        }       

        public override void QueryData()
        {
            rtvs = new List<RTV>();

            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    string storeClause = (storeCode == "ALL") ? "" : $@"storeCode=""{storeCode}"" ";
                    string criteriaXML = $@"<criteria setByFilter=""true"" {storeClause}productID="""" shipDestStatuses=""D""> <statuses> <status code=""D""/> </statuses> </criteria>";

                    SqlCommand sqlcommand = new SqlCommand("ar_sp_RTVGetByCriteria", conn);
                    sqlcommand.CommandType = CommandType.StoredProcedure;
                    sqlcommand.Parameters.Add(new SqlParameter("@i_JustCount", 0));
                    sqlcommand.Parameters.Add(new SqlParameter("@i_criteriaXml", SqlDbType.Xml) { Value = criteriaXML });
                    sqlcommand.Parameters.Add(new SqlParameter("@i_UserCode", "mgb"));
                    SqlDataReader reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        DateTime criteriaDate = (DateTime)reader["shipDate"];
                        if (!string.IsNullOrEmpty(reader["buyerName"].ToString()) && criteriaDate >= parameterDateFrom && criteriaDate <= parameterDateTo)
                        {
                            RTV rtv = new RTV();
                            rtv.RTVCode = (reader["code"] == DBNull.Value) ? "" : reader["code"].ToString();
                            rtv.Store = (reader["sourceLocCodeAndDescription"] == DBNull.Value) ? "" : reader["sourceLocCodeAndDescription"].ToString();
                            rtv.Vendor = (reader["destLocCodeAndDescription"] == DBNull.Value) ? "" : reader["destLocCodeAndDescription"].ToString();
                            rtv.Status = (reader["shipDestStatusDescription"] == DBNull.Value) ? "" : reader["shipDestStatusDescription"].ToString();
                            rtv.Buyer = (reader["buyerCodeAndDescription"] == DBNull.Value) ? "" : reader["buyerCodeAndDescription"].ToString();
                            rtv.Carrier = (reader["carrierDescription"] == DBNull.Value) ? "" : reader["carrierDescription"].ToString();
                            rtv.Description = (reader["description"] == DBNull.Value) ? "" : reader["description"].ToString();
                            rtv.CreateDate = (DateTime)reader["createDate"];
                            rtvs.Add(rtv);
                        }                        
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database read failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        public override void GenerateExcelFile()
        {
            string fileNamePrefix;
            if (basis == RTVReportBasis.CreateDate)
                fileNamePrefix = string.Format(REPORT_TITLE, "Created");
            else
                fileNamePrefix = string.Format(REPORT_TITLE, "Shipped");
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Archive\\" + DateTime.Now.ToString(@"yyyy\\MM\\dd");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string fileName = $"{fileNamePrefix.Replace(" ", string.Empty)}_{storeCode}_{parameterDateFrom.ToString("yyyyMMdd")}-{parameterDateTo.ToString("yyyyMMdd")}_000.xlsx";

            // Make sure you're writing to a new file
            Directory.CreateDirectory(directory);
            int fileNumber = 0;
            while (File.Exists(Path.Combine(directory, fileName)))
            {
                fileNumber++;
                fileName = $"{fileNamePrefix.Replace(" ", string.Empty)}_{storeCode}_{parameterDateFrom.ToString("yyyyMMdd")}-{parameterDateTo.ToString("yyyyMMdd")}_{fileNumber.ToString("D" + 3)}.xlsx";
            }

            try
            {
                // Start Excel
                Application excel = new Application();
                excel.Visible = false;
                excel.DisplayAlerts = false;
                Workbook workbook = excel.Workbooks.Add(Type.Missing);
                Worksheet worksheet = workbook.ActiveSheet;
                worksheet.Name = fileNamePrefix.Replace(" ", string.Empty);
                Range formatRange;

                //  Margins 
                worksheet.PageSetup.Orientation = XlPageOrientation.xlLandscape;
                worksheet.PageSetup.TopMargin = excel.InchesToPoints(0.75);
                worksheet.PageSetup.BottomMargin = excel.InchesToPoints(0.75);
                worksheet.PageSetup.LeftMargin = excel.InchesToPoints(0.25);
                worksheet.PageSetup.RightMargin = excel.InchesToPoints(0.25);
                worksheet.PageSetup.CenterHorizontally = true;

                // Column widths
                worksheet.Columns["A:A"].ColumnWidth = 7;
                worksheet.Columns["B:B"].ColumnWidth = 22;
                worksheet.Columns["C:C"].ColumnWidth = 23;
                worksheet.Columns["D:D"].ColumnWidth = 15;
                worksheet.Columns["E:E"].ColumnWidth = 18;
                worksheet.Columns["F:F"].ColumnWidth = 6;
                worksheet.Columns["G:G"].ColumnWidth = 18;
                worksheet.Columns["H:H"].ColumnWidth = 18;

                // Report Header
                Range headerRange = (Range)worksheet.Range[worksheet.Cells[1, 1], worksheet.Cells[1, 8]];
                headerRange.Merge();
                headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                headerRange.Font.Size = 16;
                headerRange.Font.Bold = true;
                headerRange.Font.Color = ColorTranslator.ToOle(Color.White);
                headerRange.Interior.Color = ColorTranslator.ToOle(Color.Gray);
                headerRange.RowHeight = 36;
                worksheet.Cells[1, 1] = fileNamePrefix;                
                
                // Store and Date
                worksheet.Cells[2, 2] = "Store Number:";
                worksheet.Cells[2, 3] = storeCode;                
                worksheet.Cells[3, 3] = parameterDateFrom.ToString("M/d/yyyy");                
                worksheet.Cells[4, 3] = parameterDateTo.ToString("M/d/yyyy");
                if (basis == RTVReportBasis.ShipDate)
                {
                    worksheet.Cells[3, 2] = "Ship Date From:";
                    worksheet.Cells[4, 2] = "Ship Date To:";
                }
                if (basis == RTVReportBasis.CreateDate)
                {
                    worksheet.Cells[3, 2] = "Create Date From:";
                    worksheet.Cells[4, 2] = "Create Date To:";
                }                
                formatRange = (Range)worksheet.Range[worksheet.Cells[2, 2], worksheet.Cells[4, 2]];
                formatRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                formatRange.Font.Bold = true;
                formatRange = (Range)worksheet.Range[worksheet.Cells[2, 3], worksheet.Cells[4, 3]];
                formatRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;

                // Data Column Header
                Range dataColumnHeaderRange = (Range)worksheet.Range[worksheet.Cells[6, 1], worksheet.Cells[6, 8]];
                dataColumnHeaderRange.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                dataColumnHeaderRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                dataColumnHeaderRange.Font.Size = 10;
                dataColumnHeaderRange.Font.Bold = true;
                worksheet.Cells[6, 1] = "RTV";
                worksheet.Cells[6, 2] = "Store";
                worksheet.Cells[6, 3] = "Vendor";
                worksheet.Cells[6, 4] = "Status";
                worksheet.Cells[6, 5] = "Buyer";
                worksheet.Cells[6, 6] = "Carrier";
                worksheet.Cells[6, 7] = "Description";
                worksheet.Cells[6, 8] = "Create Date";

                // Data
                int row = 7;
                foreach (RTV rtv in rtvs.OrderBy(r => r.Store).ThenBy(r => r.RTVCode))
                {
                    Range dataLineRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 8]];
                    worksheet.Cells[row, 1].NumberFormat = "@";
                    worksheet.Cells[row, 1] = rtv.RTVCode;
                    worksheet.Cells[row, 2] = rtv.Store;
                    worksheet.Cells[row, 3] = rtv.Vendor;
                    worksheet.Cells[row, 4] = rtv.Status;
                    worksheet.Cells[row, 5] = rtv.Buyer;
                    worksheet.Cells[row, 6] = rtv.Carrier;
                    worksheet.Cells[row, 7] = rtv.Description;
                    worksheet.Cells[row, 8] = rtv.CreateDate.Value.ToString("MM/dd/yyyy hh:mm tt");
                    if (row % 2 == 0)
                    {
                        dataLineRange.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(230, 230, 230));
                    }
                    row++;
                }
                Range dataRange = (Range)worksheet.Range[worksheet.Cells[7, 1], worksheet.Cells[row, 8]];
                dataRange.Font.Size = 9;
                dataRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                dataRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                dataRange.WrapText = true;
                Range createDateRange = (Range)worksheet.Range[worksheet.Cells[7, 8], worksheet.Cells[row, 8]];
                createDateRange.NumberFormat = "mm/dd/yyyy h:mm AM/PM";

                // Page Numbering
                worksheet.PageSetup.LeftFooter = fileNamePrefix;
                worksheet.PageSetup.CenterFooter = "&P/&N";                
                worksheet.PageSetup.RightFooter = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                // Save and close
                workbook.SaveAs(Path.Combine(directory, fileName));
                workbook.Close();
                excel.Quit();
            }
            catch (Exception ex)
            {
                MBWindow.Show($"Report generation failed: {ex.Message}", "Error", Utilities.MessageBoxButton.OK);
                Logger.LogError(ex);
                return;
            }            

            // Open the folder
            bool explorerOpen = false;
            Process[] processes = Process.GetProcessesByName("explorer");
            foreach (Process process in processes)
            {
                if (process.MainWindowTitle == DateTime.Now.ToString(@"dd"))
                {
                    explorerOpen = true;
                    break;
                }
            }
            if (!explorerOpen)
                Process.Start("explorer.exe", directory);
        }
    }
}
