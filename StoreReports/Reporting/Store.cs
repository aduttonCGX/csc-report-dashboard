﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreReports.Reporting
{
    public class Store
    {
        private string code;
        private string description;       

        public string Code { get => code; set => code = value; }
        public string Description { get => description; set => description = value; }

        public Store(string code, string description)
        {
            this.code = code;
            this.description = description;
        }
    }
}
