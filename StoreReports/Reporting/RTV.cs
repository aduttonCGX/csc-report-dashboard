﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreReports.Reporting
{
    public class RTV
    {
        private string rtvCode;
        private string store;
        private string vendor;
        private string status;
        private string buyer;
        private string carrier;
        private string description;
        private DateTime? createDate;
        public string RTVCode { get => rtvCode; set => rtvCode = value; }
        public string Store { get => store; set => store = value; }
        public string Vendor { get => vendor; set => vendor = value; }
        public string Status { get => status; set => status = value; }
        public string Buyer { get => buyer; set => buyer = value; }
        public string Carrier { get => carrier; set => carrier = value; }
        public string Description { get => description; set => description = value; }
        public DateTime? CreateDate { get => createDate; set => createDate = value; }
    }
}
