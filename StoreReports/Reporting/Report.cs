﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreReports.Reporting
{
    public abstract class Report
    {
        public abstract void QueryData();
        public abstract void GenerateExcelFile();
    }
}
