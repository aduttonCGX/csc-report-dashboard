﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreReports.Reporting
{
    public class VMI
    {
        #region Private Members
        private string storeDescription;
        private string vendorDescription;
        private string vmiNumber;
        private string status;
        private DateTime transactionDate;
        private string invoiceNumber;
        private double invoiceTotal;
        private List<VMILineItem> items;
        private bool isRTV;
        private string rtvCode;
        #endregion

        #region Public Properties
        public string StoreDescription { get => storeDescription; set => storeDescription = value; }
        public string VendorDescription { get => vendorDescription; set => vendorDescription = value; }
        public string VMINumber { get => vmiNumber; set => vmiNumber = value; }
        public string Status { get => status; set => status = value; }
        public DateTime TransactionDate { get => transactionDate; set => transactionDate = value; }
        public string InvoiceNumber { get => invoiceNumber; set => invoiceNumber = value; }
        public double InvoiceTotal { get => invoiceTotal; set => invoiceTotal = value; }
        public List<VMILineItem> Items { get => items; }
        public int TotalQuantity { get => items.Sum(x => x.Quantity); }
        public double TotalExtendedInvoiceCost { get => items.Sum(x => x.ExtendedInvoiceCost); }
        public bool IsRTV { get => isRTV; set => isRTV = value; }
        public string RTVCode { get => rtvCode; set => rtvCode = value; }
        #endregion

        #region Constructors
        public VMI(string storeDescription = "", string vendorDescription = "", string vmiNumber = "", string status = "", string invoiceNumber = "", double invoiceTotal = 0.00f)
        {
            this.storeDescription = storeDescription;
            this.vendorDescription = vendorDescription;
            this.vmiNumber = vmiNumber;
            this.status = status;
            this.transactionDate = DateTime.MinValue;
            this.invoiceNumber = invoiceNumber;
            this.invoiceTotal = invoiceTotal;
            items = new List<VMILineItem>();
            isRTV = false;
            rtvCode = "";
        }
        #endregion


    }
}
