﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace StoreReports.Utilities
{
    [XmlRoot(ElementName = "Session")]
    public class Session
    {
        #region Constants
        private const string FILENAME = "session.xml";
        private const float BASE_HEIGHT = 330.0f;
        private const float PURCHASE_ORDER_CONTROL_HEIGHT = 58.62f;
        private const float VMI_REGISTER_CONTROL_HEIGHT = 58.62f;
        private const float VMI_WEEKLY_CONTROL_HEIGHT = 126.48f;
        private const float TRANSFER_RECIEVE_CONTROL_HEIGHT = 58.62f;
        private const float TRANSFER_SHIP_CONTROL_HEIGHT = 58.62f;
        private const float RTV_ORDER_CONTROL_HEIGHT = 103.86f;
        #endregion

        #region Private Members
        private bool purchaseOrderReportShown;
        private bool vmiRegisterReportShown;
        private bool vmiWeeklyReportShown;
        private bool transferReceiptReportShown;
        private bool transferShipReportShown;
        private bool rtvReportShown;
        private string locationCode;
        private string directory;
        #endregion

        #region Public Properties
        [XmlElement(ElementName = "PurchaseOrderReportShown")]
        public bool PurchaseOrderReportShown { get => purchaseOrderReportShown; set => purchaseOrderReportShown = value; }
        [XmlElement(ElementName = "VMIRegisterReportShown")]
        public bool VMIRegisterReportShown { get => vmiRegisterReportShown; set => vmiRegisterReportShown = value; }
        [XmlElement(ElementName = "VMIWeeklyReportShown")]
        public bool VMIWeeklyReportShown { get => vmiWeeklyReportShown; set => vmiWeeklyReportShown = value; }
        [XmlElement(ElementName = "TransferReceiptReportShown")]
        public bool TransferReceiptReportShown { get => transferReceiptReportShown; set => transferReceiptReportShown = value; }
        [XmlElement(ElementName = "TransferShipReportShown")]
        public bool TransferShipReportShown { get => transferShipReportShown; set => transferShipReportShown = value; }
        [XmlElement(ElementName = "RTVReportShown")]
        public bool RTVReportShown { get => rtvReportShown; set => rtvReportShown = value; }
        [XmlElement(ElementName = "LocationCode")]
        public string LocationCode { get => locationCode; set => locationCode = value; }
        [XmlIgnore]
        public string Filepath { get => Path.Combine(directory, FILENAME); }
        [XmlIgnore]
        public float Height
        {
            get
            {
                float height = BASE_HEIGHT;
                height += (purchaseOrderReportShown) ? PURCHASE_ORDER_CONTROL_HEIGHT : 0.0f;
                height += (vmiRegisterReportShown) ? VMI_REGISTER_CONTROL_HEIGHT : 0.0f;
                height += (vmiWeeklyReportShown) ? VMI_WEEKLY_CONTROL_HEIGHT : 0.0f;
                height += (transferReceiptReportShown) ? TRANSFER_RECIEVE_CONTROL_HEIGHT : 0.0f;
                height += (transferShipReportShown) ? TRANSFER_SHIP_CONTROL_HEIGHT : 0.0f;
                height += (rtvReportShown) ? RTV_ORDER_CONTROL_HEIGHT : 0.0f;
                return height;
            }
        }        
        #endregion

        #region Constructors
        public Session()
        {
            directory = $"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\\StoreReports";
        }
        #endregion

        #region Methods
        public void SaveToFile()
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(Session));
                XmlWriterSettings settings = new XmlWriterSettings { Indent = true };
                using (XmlWriter s = XmlWriter.Create(Filepath, settings))
                    xs.Serialize(s, this);
            }
            catch (Exception ex)
            {
                Logger.LogError($"[{System.Reflection.MethodBase.GetCurrentMethod().Name}] Could not save {FILENAME}.  {ex.Message}");
            }
        }
        public void LoadFromFile()
        {
            if (!File.Exists(Filepath))
            {
                purchaseOrderReportShown = true;
                vmiRegisterReportShown = true;
                vmiWeeklyReportShown = true;
                transferReceiptReportShown = true;
                transferShipReportShown = true;
                rtvReportShown = true;
            }

            var serializer = new XmlSerializer(typeof(Session));
            Session temp;
            try
            {
                using (var stream = File.OpenRead(Filepath))
                {
                    temp = (Session)serializer.Deserialize(stream);
                    purchaseOrderReportShown = temp.purchaseOrderReportShown;
                    vmiRegisterReportShown = temp.vmiRegisterReportShown;
                    vmiWeeklyReportShown = temp.vmiWeeklyReportShown;
                    transferReceiptReportShown = temp.transferReceiptReportShown;
                    transferShipReportShown = temp.transferShipReportShown;
                    rtvReportShown = temp.rtvReportShown;
                    locationCode = temp.locationCode;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError($"[{System.Reflection.MethodBase.GetCurrentMethod().Name}] Could not load {FILENAME}.  {ex.Message}");
            }
        }
        #endregion
    }
}

