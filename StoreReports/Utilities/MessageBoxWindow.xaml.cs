﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StoreReports.Utilities
{
    /// <summary>
    /// Interaction logic for MessageBoxWindow.xaml
    /// </summary>
    /// 
    public static class MBWindow
    {
        public static MessageBoxResult Show(string messageBoxText, string caption, MessageBoxButton button)
        {
            MessageBoxWindow messageBoxWindow = new MessageBoxWindow(messageBoxText, caption, button);
            messageBoxWindow.ShowDialog();
            return messageBoxWindow.Result;
        }
    }
    public enum MessageBoxButton { OK = 0, OKCancel = 1, YesNoCancel = 2, YesNo = 3, None = 4 }
    public partial class MessageBoxWindow : Window
    {
        private MessageBoxResult result = MessageBoxResult.None;
        private bool buttonDepressed = false;

        public MessageBoxResult Result { get => result; set => result = value; }

        public MessageBoxWindow(string messageBoxText, string caption, MessageBoxButton button)
        {
            InitializeComponent();

            lblMessage.Text = messageBoxText;
            lblTitle.Text = caption;
            Title = lblTitle.Text;

            switch (button)
            {
                case (MessageBoxButton.OK):
                    btnOK.Visibility = Visibility.Visible;
                    btnYes.Visibility = Visibility.Collapsed;
                    btnNo.Visibility = Visibility.Collapsed;
                    btnCancel.Visibility = Visibility.Collapsed;
                    break;
                case (MessageBoxButton.OKCancel):
                    btnOK.Visibility = Visibility.Visible;
                    btnYes.Visibility = Visibility.Collapsed;
                    btnNo.Visibility = Visibility.Collapsed;
                    btnCancel.Visibility = Visibility.Visible;
                    break;
                case (MessageBoxButton.YesNoCancel):
                    btnOK.Visibility = Visibility.Collapsed;
                    btnYes.Visibility = Visibility.Visible;
                    btnNo.Visibility = Visibility.Visible;
                    btnCancel.Visibility = Visibility.Visible;
                    break;
                case (MessageBoxButton.YesNo):
                    btnOK.Visibility = Visibility.Collapsed;
                    btnYes.Visibility = Visibility.Visible;
                    btnNo.Visibility = Visibility.Visible;
                    btnCancel.Visibility = Visibility.Collapsed;
                    break;
                case (MessageBoxButton.None):
                    btnOK.Visibility = Visibility.Collapsed;
                    btnYes.Visibility = Visibility.Collapsed;
                    btnNo.Visibility = Visibility.Collapsed;
                    btnCancel.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            result = MessageBoxResult.OK;
            Close();
        }
        private void BtnYes_Click(object sender, RoutedEventArgs e)
        {
            result = MessageBoxResult.Yes;
            Close();
        }
        private void BtnNo_Click(object sender, RoutedEventArgs e)
        {
            result = MessageBoxResult.No;
            Close();
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            result = MessageBoxResult.Cancel;
            Close();
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    if (btnOK.Visibility == Visibility.Visible)
                    {
                        result = MessageBoxResult.OK;
                        Close();
                        return;
                    }
                    if (btnYes.Visibility == Visibility.Visible)
                    {
                        result = MessageBoxResult.Yes;
                        Close();
                        return;
                    }
                    break;
                case Key.Escape:
                    if (btnCancel.Visibility == Visibility.Visible)
                    {
                        result = MessageBoxResult.Cancel;
                        Close();
                        return;
                    }
                    if (btnNo.Visibility == Visibility.Visible)
                    {
                        result = MessageBoxResult.No;
                        Close();
                        return;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
